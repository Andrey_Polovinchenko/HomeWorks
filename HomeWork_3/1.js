/*

  Задание написать простой слайдер.

    Есть массив с картинками из которых должен состоять наш слайдер.
    По умолчанию выводим первый элмемнт нашего слайдера в блок с id='slider'
    ( window.onload = func(){...} // window.addEventListener('load', function(){...}) )
    По клику на PrevSlide/NextSlide показываем предыдущий или следующий сладй соответствено.

    Для этого необходимо написать 4 функции которые будут:

    1. Срабатывать на событие load обьекта window и загружать наш слайд по умолчанию.
    2. RenderImage -> Очищать текущий контент блока #slider. Создавать нашу картинку и через метод аппенд чайлд вставлять её в слайдер.
    3. NextSlide -> По клику на NextSilde передавать currentPosition текущего слайда + 1 в функцию рендера
    4. PrevSlide -> Тоже самое что предыдущий вариант, но на кнопку PrevSlide и передавать currentPosition - 1
      + бонус сделать так что бы при достижении последнего и первого слада вас после кидало на первый и последний слайд соответственно.
      + бонсу анимация появления слайдов через навешивание дополнительных стилей

*/

  var OurSliderImages = ['images/cat1.jpg', 'images/cat2.jpg', 'images/cat3.jpg', 'images/cat4.jpg', 'images/cat5.jpg', 'images/cat6.jpg', 'images/cat7.jpg', 'images/cat8.jpg'];
  var currentPosition = 0;

function startSlider() {
  var getCont = document.getElementById('slider');
  var getFirstSlide = OurSliderImages[0];
  var createImg = document.createElement('img');
  createImg.setAttribute('src', getFirstSlide);
  getCont.appendChild(createImg);
}

window.addEventListener('load', startSlider);

function renderImage() {
	document.getElementById('slider').innerHTML = " ";
	var getCont = document.getElementById('slider');
	var createImg = document.createElement('img');
	createImg.setAttribute('src', OurSliderImages[currentPosition]);
	getCont.appendChild(createImg);
}

function funcNextSlide() {
	currentPosition++;
	console.log(currentPosition);
	if (currentPosition == 8) {
		currentPosition = 0;
	}
}
function funcPrevSlide() {
	currentPosition--;
	console.log(currentPosition);
	if (currentPosition == -1) {
		currentPosition = 7;
	}
}

var getHeader = document.getElementById('SliderControls');
getHeader.addEventListener('click', renderImage);

var nextSilde = document.getElementById('NextSilde');
nextSilde.addEventListener('click', funcNextSlide);

var prevSilde = document.getElementById('PrevSilde');
prevSilde.addEventListener('click', funcPrevSlide);


