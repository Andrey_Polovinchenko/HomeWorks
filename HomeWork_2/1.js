
  /*

    Задание 1.

    Написать скрипт который будет будет переключать вкладки по нажатию
    на кнопки в хедере.

    Главное условие - изменять файл HTML нельзя.

    Алгоритм:
      1. Выбрать каждую кнопку в шапке
         + бонус выбрать одним селектором

      2. Повесить кнопку онклик
          button1.onclick = function(event) {

          }
          + бонус: один обработчик на все три кнопки

      3. Написать функцию которая выбирает соответствующую вкладку
         и добавляет к ней класс active

      4. Написать функцию hideAllTabs которая прячет все вкладки.
         Удаляя класс active со всех вкладок

    Методы для работы:

      getElementById
      querySelector
      classList
      classList.add
      forEach
      onclick

      element.onclick = function(event) {
        // do stuff ...
      }

  */

var getHeader = document.getElementById('buttonContainer');
var getButton = document.getElementsByClassName('showButton');
var array = [].slice.call(getButton);
getHeader.onclick = function(event) {
	    array.forEach(function() {
	    	var numberAttr = event.target.getAttribute('data-tab');
	    	switch (numberAttr) {
	    	  case '1':
	    	    document.querySelectorAll('.tab')[0].classList.add('active');
	    	    document.querySelectorAll('.tab')[1].classList.remove('active');
	    	    document.querySelectorAll('.tab')[2].classList.remove('active');
	    	    break;
	    	  case '2':
	    	    document.querySelectorAll('.tab')[1].classList.add('active');
	    	    document.querySelectorAll('.tab')[0].classList.remove('active');
	    	    document.querySelectorAll('.tab')[2].classList.remove('active');
	    	    break;
	    	  case '3':
	    	    document.querySelectorAll('.tab')[2].classList.add('active');
	    	    document.querySelectorAll('.tab')[1].classList.remove('active');
	    	    document.querySelectorAll('.tab')[0].classList.remove('active');
	    	    break;
	    	  case '4':
	    	    document.querySelectorAll('.tab')[0].classList.remove('active');
	    	    document.querySelectorAll('.tab')[1].classList.remove('active');
	    	    document.querySelectorAll('.tab')[2].classList.remove('active');
	    	    break;
	    	}
	    })
};