/*

  Задание 1.

  Написать функцию которая будет задавать СЛУЧАЙНЫЙ цвет для фона.
  Каждая перезагрузка страницы будет с новым цветом.
  Для написания используйте функцию на получение случайного целого числа,
  между минимальным и максимальным значением (Приложена снизу задания)

  + Бонус, повесить обработчик на кнопку через метод onClick
  + Бонус, использовать 6-ричную систему исчесления и цвет HEX -> #FFCC00
  + Бонус выводить полученый цвет по центру страницы.
  Необходимо создать блок через createElement задать ему стили через element.style
  и вывести через appendChild или insertBefore


  Необходимые материалы:
    Math.Random (Доки): https://developer.mozilla.org/uk/docs/Web/JavaScript/Reference/Global_Objects/Math/random
    function getRandomIntInclusive(min, max) {
      min = Math.ceil(min);
      max = Math.floor(max);
      return Math.floor(Math.random() * (max - min + 1)) + min;
    }
    __
    Работа с цветом:
    Вариант 1.
      Исользовать element.style.background = 'rgb(r,g,b)';
      где r,g,b случайное число от 0 до 256;

    Вариант 2.
      Исользовать element.style.background = '#RRGGBB';
      где, RR, GG, BB, значние цвета в 16-ричной системе исu.wikipedia.org/wiki/%D0чесления
      Формирование цвета в вебе: https://r%A6%D0%B2%D0%B5%D1%82%D0%B0_HTML
      Перевод в 16-ричную систему исчесления делается при помощи
      метода Number.toString( 16 ) https://www.w3schools.com/jsref/jsref_tostring_number.asp,

      var myNumber = '251'
      myNumber.toString(16) // fb

*/

/*
    Задание 2.

    Сложить в элементе с id App следующую размету HTML:

    <header>
      <a href="http://google.com.ua">
        <img src="https://www.google.com.ua/images/branding/googlelogo/1x/googlelogo_color_272x92dp.png">
      </a>
      <div class="menu">
        <a href="#1"> link 1</a>
        <a href="#1"> link 2</a>
        <a href="#1"> link 3</a>
      </div>
    </header>


    Используя следующие методы для работы:
    getElementById
    createElement
    element.innerText
    element.className
    element.setAttribute
    element.appendChild

*/


// TASK_N2
// var getApp = document.getElementById('app');
// var createHead = document.createElement('header');
// var headerFull = getApp.appendChild(createHead);

// var createLink = document.createElement('a');
// var linkAttribute = createLink.setAttribute('href', 'http://google.com.ua');
// var img = document.createElement('img');
// var imgAttr = img.setAttribute('src', 'https://www.google.com.ua/images/branding/googlelogo/1x/googlelogo_color_272x92dp.png');
// createLink.appendChild(img);
// headerFull.appendChild(createLink);

// var createMenu = document.createElement('ul');
// var menuAttr = createMenu.setAttribute('class', 'menu');
// headerFull.appendChild(createMenu);

// for (i = 0; i < 3; i++) {
//   createMenu.innerHTML += "<a href='#1'></a>";
// }

// var getMenuLink = document.querySelectorAll('.menu a')[0];
// getMenuLink.innerText = "link 1";

// var getMenuLink2 = document.querySelectorAll('.menu a')[1];
// getMenuLink2.innerText = "link 2";

// var getMenuLink3 = document.querySelectorAll('.menu a')[2];
// getMenuLink3.innerText = "link 3";


// TASK_N1

function getRandomIntInclusive(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

function setColor(min=1, max=256) {
  var r = getRandomIntInclusive(min, max);
  var g = getRandomIntInclusive(min, max);
  var b = getRandomIntInclusive(min, max);
  document.body.style.background = `rgb(${r},${g},${b})`;
}

setColor();


